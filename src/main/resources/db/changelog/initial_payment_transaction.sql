CREATE DATABASE  IF NOT EXISTS `payment_transaction`
    DEFAULT CHARACTER set UTF8mb4 COLLATE utf8mb4_bin;
USE `payment_transaction`;

CREATE TABLE IF NOT EXISTS `transaction` (
                                             `transaction_id` varchar(255) NOT NULL,
                                             `amount` double NOT NULL,
                                             `create_date` timestamp DEFAULT CURRENT_TIMESTAMP,
                                             `currency` varchar(255),
                                             `customer_uuid` varchar(255),
                                             `order_description` varchar(255),
                                             `order_reference` varchar(255),
                                             `payment_date` datetime,
                                             `payment_failure` varchar(255),
                                             `payment_gateway_reference` varchar(255),
                                             `payment_gateway_token` varchar(255),
                                             `reference` varchar(255),
                                             `status` varchar(255) NOT NULL,
                                             `timestamp` timestamp DEFAULT CURRENT_TIMESTAMP,
                                             PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
