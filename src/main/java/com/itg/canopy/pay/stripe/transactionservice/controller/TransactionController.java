/*
 * Copyright (c) 2019. ITG Ltd
 */

package com.itg.canopy.pay.stripe.transactionservice.controller;

import com.itg.canopy.pay.stripe.transactionservice.dto.StripeRequest;
import com.itg.canopy.pay.stripe.transactionservice.dto.StripeResponse;
import com.itg.canopy.pay.stripe.transactionservice.dto.TransactionV1;
import com.itg.canopy.pay.stripe.transactionservice.exception.TransactionException;
import com.itg.canopy.pay.stripe.transactionservice.service.PaymentService;
import com.itg.canopy.pay.stripe.transactionservice.service.TransactionErrorMessageConfig;
import com.itg.canopy.pay.stripe.transactionservice.service.TransactionService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "v1/payment-transactions")
public class TransactionController {
    private final TransactionService transactionService;
    private final TransactionErrorMessageConfig errorMessageConfig;
    private final PaymentService paymentService;

    public TransactionController(TransactionService transactionService, PaymentService paymentService, TransactionErrorMessageConfig errorMessageConfig) {
        this.transactionService = transactionService;
        this.paymentService = paymentService;
        this.errorMessageConfig = errorMessageConfig;
    }

    @ApiOperation(value = "",authorizations = {@Authorization(value = "auth0_jwt")})
    @PostMapping(value = "")
    public ResponseEntity saveTransaction(@Valid @RequestBody TransactionV1 transactionV1, BindingResult result) throws Exception {
        if (result.hasErrors())
            throw new TransactionException(errorMessageConfig.getTransactionObjectInvalidMessage());
        transactionService.saveTransaction(transactionV1);
        return new ResponseEntity(HttpStatus.OK);
    }

    @ApiOperation(value = "/{transactionId}",authorizations = {@Authorization(value = "auth0_jwt")})
    @GetMapping(value = "/{transactionId}")
    public ResponseEntity<TransactionV1> getTransaction(@PathVariable String transactionId) throws Exception{
        TransactionV1 transactionV1 = transactionService.getTransactionById(transactionId);
        return new ResponseEntity<>(transactionV1, HttpStatus.OK);
    }

    @ApiOperation(value = "",authorizations = {@Authorization(value = "auth0_jwt")})
    @PutMapping(value = "")
    public ResponseEntity updateTransaction(@Valid @RequestBody TransactionV1 transactionV1, BindingResult result) throws Exception {
        if (result.hasErrors())
            throw new TransactionException(errorMessageConfig.getTransactionObjectInvalidMessage());
        transactionService.updateTransaction(transactionV1);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiOperation(value = "/{transactionReference}/MakePayment",authorizations = {@Authorization(value = "auth0_jwt")})
    @PostMapping(value = "/{transactionReference}/MakePayment")
    public ResponseEntity<StripeResponse> processPayment(@PathVariable String transactionReference, @Valid @RequestBody StripeRequest stripeRequest, BindingResult result) throws Exception {
        if (result.hasErrors())
            throw new TransactionException("Payment object is not correct");

        StripeResponse response = paymentService.authenticatePayment(stripeRequest);

        if (response == null || response.getError() != null)
            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);

        return new ResponseEntity(response, HttpStatus.OK);
    }
}
