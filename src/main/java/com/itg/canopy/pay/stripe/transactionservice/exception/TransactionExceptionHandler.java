/*
 * Copyright (c) 2019. ITG Ltd
 */

package com.itg.canopy.pay.stripe.transactionservice.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ControllerAdvice
@Slf4j
public class TransactionExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({TransactionException.class})
        public void handleBadRequest(TransactionException exception, HttpServletResponse response) throws IOException {
        log.error("Exception: ", exception);
        response.sendError(HttpStatus.BAD_REQUEST.value());
    }

    @ExceptionHandler({Exception.class})
    public void handleInternalServerErrorRequest(Exception exception,  HttpServletResponse response) throws IOException {
        log.error("Exception: ", exception);
        response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value());
    }


}
