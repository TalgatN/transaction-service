/*
 * Copyright (c) 2019. ITG Ltd
 */

package com.itg.canopy.pay.stripe.transactionservice.repo;

import com.itg.canopy.pay.stripe.transactionservice.dto.TransactionV1;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TransactionRepo extends JpaRepository<TransactionV1,String> {

    @Query(value = "SELECT * FROM transaction WHERE reference =:transactionReference", nativeQuery = true)
    public TransactionV1 findByRerefence(@Param("transactionReference") String transactionReference);
}
