package com.itg.canopy.pay.stripe.transactionservice.service;


import com.itg.canopy.pay.stripe.transactionservice.dto.StripeRequest;
import com.itg.canopy.pay.stripe.transactionservice.dto.StripeResponse;

public interface PaymentService {

    public StripeResponse authenticatePayment(StripeRequest request);
}
