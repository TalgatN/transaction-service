/*
 * Copyright (c) 2019. ITG Ltd
 */

package com.itg.canopy.pay.stripe.transactionservice.exception;

public class TransactionException extends Exception {
    public TransactionException(String exceptionMessage) {
        super(exceptionMessage);
    }
}
