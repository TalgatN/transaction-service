package com.itg.canopy.pay.stripe.transactionservice.service;



import com.itg.canopy.pay.stripe.transactionservice.dto.StripeRequest;
import com.itg.canopy.pay.stripe.transactionservice.dto.StripeResponse;
import com.itg.canopy.pay.stripe.transactionservice.dto.TransactionV1;
import com.itg.canopy.pay.stripe.transactionservice.repo.TransactionRepo;
import com.stripe.Stripe;
import com.stripe.model.PaymentIntent;
import com.stripe.param.PaymentIntentCreateParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class StripePaymentServiceImpl implements PaymentService {

    @Value("${stripe.key}")
    private String key;

    @Autowired
    TransactionService transactionService;


    @Override
    public StripeResponse authenticatePayment(StripeRequest request){

        Stripe.apiKey = key;
        PaymentIntent intent = new PaymentIntent();

        long value = (long) (request.getValue() * 100);
        log.info("VALUE-----" + value);

        try {

            if (request.getPaymentId() != null) {

                Map<String, Object> tokenMap = new HashMap<>();
                tokenMap.put("token", request.getPaymentId());

                Map<String, Object> paymentMethodData = new HashMap<>();
                paymentMethodData.put("type", "card");
                paymentMethodData.put("card", tokenMap);


                PaymentIntentCreateParams createParams = PaymentIntentCreateParams.builder()
                        .setAmount(value)
                        .setCurrency("gbp")
                        .setConfirm(true)
                        .putExtraParam("payment_method_data", paymentMethodData)
                        .setConfirmationMethod(PaymentIntentCreateParams.ConfirmationMethod.MANUAL)
                        .putExtraParam("setup_future_usage", "on_session")
                        .build();
                intent = PaymentIntent.create(createParams);
            } else if (request.getPaymentIntentId() != null) {
                intent = PaymentIntent.retrieve(request.getPaymentIntentId());
                intent = intent.confirm();
            }
            StripeResponse stripeResponse = generatePaymentResponse(intent, request.getTransactionReference());
            log.info(intent.getId());
            return stripeResponse;

        }catch (Exception e){
            StripeResponse stripeResponse = new StripeResponse();
            stripeResponse.setError(e.toString());
            log.error(e.getMessage());
            return stripeResponse;
        }

    }


    private StripeResponse generatePaymentResponse(PaymentIntent intent, String transactionReference) throws Exception{

        TransactionV1 transaction = transactionService.getTransactionByReference(transactionReference);
        transaction.setPaymentDate(new Date());
        transaction.setPaymentGatewayReference(intent.getId());

        StripeResponse stripeResponse = new StripeResponse();

        log.info(intent.getStatus());

        if (intent.getStatus().equals("requires_action")
                && intent.getNextAction().getType().equals("use_stripe_sdk")) {
            stripeResponse.setRequires_action(true);
            stripeResponse.setPayment_intent_client_secret(intent.getClientSecret());

        } else if (intent.getStatus().equals("succeeded")) {
            stripeResponse.setSuccess(true);

            transaction.setStatus("succeeded");

        } else {
            // invalid status
            stripeResponse.setError("Invalid status");

            transaction.setStatus("failed");
            transaction.setPaymentFailure(intent.getCancellationReason());

            transactionService.updateTransaction(transaction);

            return stripeResponse;
        }

        transactionService.updateTransaction(transaction);

        return stripeResponse;
    }


}
