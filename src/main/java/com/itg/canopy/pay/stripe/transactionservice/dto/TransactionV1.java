/*
 * Copyright (c) 2019. ITG Ltd
 */

package com.itg.canopy.pay.stripe.transactionservice.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "Transaction")
@ApiModel
@Getter@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionV1 {

    @ApiModelProperty(notes = "unique transaction ID")
    @Id
    private String transactionId;
    @ApiModelProperty(notes = "transaction reference")
    @NotEmpty
    private String reference;
    @ApiModelProperty(notes = "unique customer identifier")
    private String customerUuid;
    @ApiModelProperty(notes = "stripe reference")
    private String paymentGatewayReference;
    @ApiModelProperty(notes = "token generated by stripe")
    private String paymentGatewayToken;
    @ApiModelProperty(notes = "reference for the order")
    private String orderReference;
    @ApiModelProperty(notes = "description of products")
    private String orderDescription;
    @ApiModelProperty(notes = "currency of payment")
    private String currency;
    @ApiModelProperty(notes = "total amount of transaction")
    private Double amount;
    @ApiModelProperty(notes = "current transaction stauts: failed, pending, completed")
    private String status;
    @ApiModelProperty(notes = "reason for payment failure")
    private String paymentFailure;
    @ApiModelProperty(notes = "date of object creation")
    private Date createDate;
    @ApiModelProperty(notes = "date of payment submitted")
    private Date paymentDate;
    @ApiModelProperty(notes = "timestamp of transaction")
    private Timestamp timestamp;

    public TransactionV1() {
        //
    }

    @Override
    public String toString() {
        return "TransactionV1{" +
                "transactionId='" + transactionId + '\'' +
                ", reference='" + reference + '\'' +
                ", customerUuid='" + customerUuid + '\'' +
                ", paymentGatewayReference='" + paymentGatewayReference + '\'' +
                ", paymentGatewayToken='" + paymentGatewayToken + '\'' +
                ", orderReference='" + orderReference + '\'' +
                ", orderDescription='" + orderDescription + '\'' +
                ", currency='" + currency + '\'' +
                ", amount=" + amount +
                ", status='" + status + '\'' +
                ", paymentFailure='" + paymentFailure + '\'' +
                ", createDate=" + createDate +
                ", paymentDate=" + paymentDate +
                ", timestamp=" + timestamp +
                '}';
    }
}
