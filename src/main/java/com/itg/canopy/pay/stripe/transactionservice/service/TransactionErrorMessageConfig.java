/*
 * Copyright (c) 2019. ITG Ltd
 */

package com.itg.canopy.pay.stripe.transactionservice.service;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Getter
public class TransactionErrorMessageConfig {
    @Value("${transaction.object.invalid.error}")
    private String transactionObjectInvalidMessage;
    @Value("${transaction.not.found.error}")
    private String transactionNotFoundErrorMessage;
    @Value("${transaction.conflict.error}")
    private String transactionConflictErrorMessage;
}
