/*
 * Copyright (c) 2019. ITG Ltd
 */

package com.itg.canopy.pay.stripe.transactionservice.service;

import com.itg.canopy.pay.stripe.transactionservice.dto.TransactionV1;

public interface TransactionService {

    void saveTransaction(TransactionV1 transactionV1) throws Exception;

    TransactionV1 getTransactionById(String transactionId) throws Exception;

    TransactionV1 getTransactionByReference(String transactionId) throws Exception;

    void updateTransaction(TransactionV1 transactionV1) throws Exception;
}
