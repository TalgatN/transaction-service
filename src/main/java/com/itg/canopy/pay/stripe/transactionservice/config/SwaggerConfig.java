/*
 * Copyright (c) 2019. ITG Ltd
 */

package com.itg.canopy.pay.stripe.transactionservice.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.OAuthBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static springfox.documentation.builders.PathSelectors.regex;



@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Value("${swagger.host}")
    private String host;
    @Value("${swagger.basepackage}")
    private String basePackage;
    @Value("${swagger.login.endpoint}")
    private String loginEndpoint;
    @Value("${swagger.login.token}")
    private String token;
    @Value("${swagger.oath.name}")
    private String oathName;

    private SecurityScheme securityScheme(){
        List<GrantType> grantTypes = new ArrayList<>();
        ImplicitGrant implicitGrant = new ImplicitGrant(new LoginEndpoint(host + loginEndpoint), token);
        grantTypes.add(implicitGrant);
        SecurityScheme oauth = new OAuthBuilder().name("auth0_jwt").grantTypes(grantTypes).build();
        return oauth;
    }


    @Bean
    public Docket paymentService(){

        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage(basePackage))
                .paths(regex("/.*"))
                .build()
                .host(host)
                .apiInfo(metaData()).securitySchemes(Arrays.asList(securityScheme()));

    }

    private ApiInfo metaData() {
        ApiInfo apiInfo = new ApiInfo(
                "Payment Transaction Service API",
                "Payment Transaction Service API",
                "1.0",
                "Terms of service",
                new Contact("Kashif Abro", "https://springframework.guru/about/", "kashifabro@itg.co.uk"),
                "Apache License Version 2.0",
                "https://www.apache.org/licenses/LICENSE-2.0");
        return apiInfo;
    }


}

