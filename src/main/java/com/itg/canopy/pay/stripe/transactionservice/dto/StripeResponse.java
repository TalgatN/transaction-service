package com.itg.canopy.pay.stripe.transactionservice.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StripeResponse {

    private boolean requires_action;
    private String payment_intent_client_secret;
    private boolean success;
    private String Error;


    public StripeResponse() {
        //default constructor
    }

    @Override
    public String toString() {
        return "StripeResponse{" +
                "requires_action='" + requires_action + '\'' +
                ", payment_intent_client_secret='" + payment_intent_client_secret + '\'' +
                ", success=" + success +
                ", Error='" + Error + '\'' +
                '}';
    }
}
