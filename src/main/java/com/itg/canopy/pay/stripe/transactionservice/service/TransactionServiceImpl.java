/*
 * Copyright (c) 2019. ITG Ltd
 */

package com.itg.canopy.pay.stripe.transactionservice.service;

import com.itg.canopy.pay.stripe.transactionservice.dto.TransactionV1;
import com.itg.canopy.pay.stripe.transactionservice.exception.TransactionException;
import com.itg.canopy.pay.stripe.transactionservice.repo.TransactionRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class TransactionServiceImpl implements TransactionService {
    private final TransactionRepo transactionRepo;
    private final TransactionErrorMessageConfig errorMessageConfig;

    public TransactionServiceImpl(TransactionRepo transactionRepo, TransactionErrorMessageConfig errorMessageConfig) {
        this.transactionRepo = transactionRepo;
        this.errorMessageConfig = errorMessageConfig;
    }

    @Override
    public void saveTransaction(TransactionV1 transactionV1) throws Exception {
        if (transactionRepo.existsById(transactionV1.getTransactionId()))
            throw new TransactionException(errorMessageConfig.getTransactionConflictErrorMessage());

        log.info("Saving transactionV1 object with the ID: " + transactionV1.getTransactionId());
        transactionRepo.save(transactionV1);
    }

    @Override
    public TransactionV1 getTransactionById(String transactionId) throws Exception {
        log.info("reading transaction object with the ID: " + transactionId);
        return transactionRepo.findById(transactionId).orElseThrow(() -> new TransactionException(errorMessageConfig.getTransactionNotFoundErrorMessage()));
    }

    @Override
    public TransactionV1 getTransactionByReference(String transactionReference) throws Exception {
        log.info("reading transaction object with the reference: " + transactionReference);

        TransactionV1 transaction = transactionRepo.findByRerefence(transactionReference);

        if (transaction == null)
            throw new TransactionException("Transaction not found");

        return transaction;
    }

    @Override
    public void updateTransaction(TransactionV1 transactionV1) throws Exception {
        if (!transactionRepo.existsById(transactionV1.getTransactionId()))
            throw new TransactionException(errorMessageConfig.getTransactionNotFoundErrorMessage());
        log.info("Updating transactionV1 object with the ID: " + transactionV1.getTransactionId());
        transactionRepo.save(transactionV1);
    }
}
