package com.itg.canopy.pay.stripe.transactionservice.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel
@Getter
@Setter
public class StripeRequest {

    @ApiModelProperty(notes = "Token ID generated by Stripe")
    private String paymentId;
    @ApiModelProperty(notes = "Payment Intent ID for retrying a charge")
    private String paymentIntentId;
    @ApiModelProperty(notes = "Value wished to charge customer")
    private Double value;
    @ApiModelProperty(notes = "Transaction ID")
    private String transactionReference;

    public StripeRequest() {
        //default constructor
    }

    @Override
    public String toString() {
        return "StripeRequest{" +
                "paymentId='" + paymentId + '\'' +
                ", paymentIntentId='" + paymentIntentId + '\'' +
                ", value=" + value +
                ", transactionReference='" + transactionReference + '\'' +
                '}';
    }
}
