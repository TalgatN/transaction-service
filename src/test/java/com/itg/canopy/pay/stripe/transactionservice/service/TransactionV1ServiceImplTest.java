/*
 * Copyright (c) 2019. ITG Ltd
 */

package com.itg.canopy.pay.stripe.transactionservice.service;

import com.itg.canopy.pay.stripe.transactionservice.dto.TransactionV1;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class TransactionV1ServiceImplTest {

    @Autowired
    TransactionService transactionService;

    @Test
    public void saveTransaction() throws Exception {
        TransactionV1 transactionV1 = new TransactionV1();
        transactionV1.setTransactionId("test");
        transactionV1.setReference("test");

        transactionService.saveTransaction(transactionV1);

        TransactionV1 response = transactionService.getTransactionById("test");

        assertEquals("test",response.getTransactionId());
    }

    @Test
    public void getTransactionById() throws Exception {

        TransactionV1 response = transactionService.getTransactionById("1");

        assertEquals("1", response.getTransactionId());
    }

    @Test
    public void getTransactionByReference() throws Exception {

        TransactionV1 response = transactionService.getTransactionByReference("12345");

        assertEquals("1", response.getTransactionId());
    }

    @Test
    public void updateTransaction() throws Exception {

        TransactionV1 transactionV1 = new TransactionV1();
        transactionV1.setTransactionId("1");
        transactionV1.setReference("test");

        transactionService.updateTransaction(transactionV1);

        TransactionV1 response = transactionService.getTransactionByReference("test");

        assertEquals("1", response.getTransactionId());
    }
}