/*
 * Copyright (c) 2019. ITG Ltd
 */

package com.itg.canopy.pay.stripe.transactionservice;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TransactionV1ServiceApplicationTests {

	@Test
	public void contextLoads() {
	}

}
