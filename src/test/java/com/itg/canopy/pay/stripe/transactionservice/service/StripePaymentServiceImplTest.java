package com.itg.canopy.pay.stripe.transactionservice.service;

import com.itg.canopy.pay.stripe.transactionservice.controller.TransactionController;
import com.itg.canopy.pay.stripe.transactionservice.dto.StripeResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@WebMvcTest(PaymentService.class)
public class StripePaymentServiceImplTest {

    @MockBean
    private TransactionService transactionService;
    @MockBean
    private StripePaymentServiceImpl paymentService;
    @Autowired
    private MockMvc mockMvc;


    @Test
    public void authenticatePaymentTest() {

        StripeResponse stripeResponse = new StripeResponse();
        stripeResponse.setSuccess(true);

        when(paymentService.authenticatePayment(any())).thenReturn(stripeResponse);

        paymentService.authenticatePayment(any());

        assertEquals(true, stripeResponse.isSuccess());

    }

    @Test
    public void generatePaymentResponseTest() {

        StripeResponse stripeResponse = new StripeResponse();
        stripeResponse.setSuccess(true);

        when(paymentService.authenticatePayment(any())).thenReturn(stripeResponse);

        paymentService.authenticatePayment(any());

        assertEquals(true, stripeResponse.isSuccess());

    }
}