package com.itg.canopy.pay.stripe.transactionservice.repo;

import com.itg.canopy.pay.stripe.transactionservice.dto.TransactionV1;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TransactionRepoTest {

    @Autowired
    TransactionRepo transactionRepo;

    @Test
    public void findByRerefence() {

        TransactionV1 transaction = transactionRepo.findByRerefence("12345");

        assertEquals("1", transaction.getTransactionId());

    }
}