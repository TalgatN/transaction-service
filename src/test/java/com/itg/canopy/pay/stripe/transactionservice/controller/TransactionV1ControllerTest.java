/*
 * Copyright (c) 2019. ITG Ltd
 */

package com.itg.canopy.pay.stripe.transactionservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itg.canopy.pay.stripe.transactionservice.dto.StripeResponse;
import com.itg.canopy.pay.stripe.transactionservice.dto.TransactionV1;
import com.itg.canopy.pay.stripe.transactionservice.exception.TransactionException;
import com.itg.canopy.pay.stripe.transactionservice.service.PaymentService;
import com.itg.canopy.pay.stripe.transactionservice.service.TransactionErrorMessageConfig;
import com.itg.canopy.pay.stripe.transactionservice.service.TransactionService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TransactionController.class)
public class TransactionV1ControllerTest {
    @MockBean
    private TransactionService transactionService;
    @MockBean
    private TransactionErrorMessageConfig errorMessageConfig;
    @MockBean
    private PaymentService paymentService;
    @Autowired
    private MockMvc mockMvc;
    private TransactionV1 transactionV1;
    private String transactionJsonPath = "/transaction/transaction.json";

    @Before
    public void setup() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        this.transactionV1 = objectMapper.readValue(getClass().getResourceAsStream((transactionJsonPath)), TransactionV1.class);
    }
    //POST
    @Test
    public void saveTransactionSuccessTest() throws Exception {

        doNothing().when(transactionService).saveTransaction(any());

        mockMvc.perform(MockMvcRequestBuilders
                .post("/v1/payment-transactions")
                .content(asJsonString(transactionV1))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void saveTransactionBadRequestExceptionTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/v1/payment-transactions")
                .content(asJsonString("{}"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void saveTransactionConflictExceptionTest() throws Exception {
        //when(transactionService.saveTransaction(any())).thenThrow(TransactionExistException.class);

        doThrow(TransactionException.class).when(transactionService).saveTransaction(any());

        mockMvc.perform(MockMvcRequestBuilders
                .post("/v1/payment-transactions")
                .content(asJsonString(transactionV1))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    //PUT
    @Test
    public void updateTransactionSuccessTest() throws Exception {
        //when(transactionService.updateTransaction(transactionV1)).thenReturn("success");

        doNothing().when(transactionService).updateTransaction(any());

        mockMvc.perform(MockMvcRequestBuilders
                .put("/v1/payment-transactions")
                .content(asJsonString(transactionV1))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void updateTransactionNotFoundTest() throws Exception {
       // when(transactionService.updateTransaction(any())).thenThrow(TransactionNotFoundException.class);

        doThrow(TransactionException.class).when(transactionService).updateTransaction(any());

        mockMvc.perform(MockMvcRequestBuilders
                .put("/v1/payment-transactions")
                .content(asJsonString(transactionV1))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    //GET
    @Test
    public void getTransactionSuccessTest() throws Exception {
        //given
        String transactionId = "1";

        //when
        when(transactionService.getTransactionById(transactionId)).thenReturn(any());

        //then
        mockMvc.perform(MockMvcRequestBuilders
                .get("/v1/payment-transactions/" + transactionId)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getTransactionNotFoundTest() throws Exception {
        //given
        String transactionId = "2";

        //when
        when(transactionService.getTransactionById(transactionId)).thenThrow(TransactionException.class);

        //then
        mockMvc.perform(MockMvcRequestBuilders
                .get("/v1/payment-transactions/" + transactionId)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    //POST
    @Test
    public void processPaymentSuccessTest() throws Exception {

        StripeResponse stripeResponse = new StripeResponse();

        stripeResponse.setSuccess(true);

        when(paymentService.authenticatePayment(any())).thenReturn(stripeResponse);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/v1/payment-transactions/123/MakePayment")
                .content(asJsonString(transactionV1))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void processPaymentBadRequestExceptionTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/v1/payment-transactions/123/MakePayment")
                .content(asJsonString("{}"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    private String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}