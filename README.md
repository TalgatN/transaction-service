**Localhost setup**
Sonar:
1. download and unzip SonarQube in to opt/ folder (or wherever preferred)
2. add global <settings> to settings.xml located in .m2 folder/create one
3. run sonar from the command line such as: `opt/sonarqube/bin/macosx-universal-64/sonar.sh console`
4. in the browser go to `http://localhost:9000` (default login admin admin) and 
    follow the on screen instruction to generate a token
5. from the root directory of the project run 
    `mvn sonar:sonar`
    or if running the first time
    `mvn sonar:sonar -Dsonar.host.url=http://localhost:9000 -Dsonar.login=the-generated-token`
6. go to `http://localhost:9000/projects` to see the report
7. to stop the sonar running in the console press ctrl + c

Liquibase:
Assuming MySql is installed on your machine, setup payment_transaction db on your localhost using 
initial_payment_transaction.sql 
(latest dump from GCP: mysqldump -d -h 35.246.121.184 payment_transaction -u root -p > dump.sql)
Also run the latest sql script in /src/main/resources/db/changelog/changelog.mysql.sql

After making changes to your localhost db schema:
to generate changelog run in the command line: mvn liquibase:generateChangeLog. This will serialise your local
db schema into sql DDL and save into changelog.mysql.sql under resources/db/changelog/ .
to update the target schema using changelog set spring.liquibase.enabled=true in the application.properties.
